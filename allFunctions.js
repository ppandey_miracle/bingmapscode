var clusterLayer;
function getClusterByPins(eachSetofCluster) {
//console.log(id);
  //console.log(eachSetofCluster[5]);
  var testVar;
  var pinsToBePassedToCluster = [];
  var newCluster;
  var val;
  Microsoft.Maps.loadModule("Microsoft.Maps.Clustering",
    function() {
        map.layers.remove(clusterLayer);


      //  map.layers.remove(clusterLayer);
        for (var i = 0; i < eachSetofCluster.length; i++) {
          val = eachSetofCluster[i];
          var position = new Microsoft.Maps.Location(val.Latitude, val.Longitude);
         /* var pushPin = new Microsoft.Maps.Pushpin(position, {
            color: "red",
            anchor: new Microsoft.Maps.Point(12, 39)
          }); */
		  var pushPin = createFontPushpin(position, "\uF041", 'FontAwesome', 30, 'black');
          pushPin.metadata = eachSetofCluster[i];
          pinsToBePassedToCluster.push(pushPin);

        } 
		
        clusterLayer = new Microsoft.Maps.ClusterLayer(pinsToBePassedToCluster);
        testVar = clusterLayer
      //  console.log("I am the cluster");
      //  console.log(clusterLayer);

        //Add a click event to the cluster layer to open an infobox.
        Microsoft.Maps.Events.addHandler(clusterLayer, 'click', showInfobox);
        //    newCluster=clusterLayer;
    //    console.log("Hi I am new cluster");
    //    console.log(clusterLayer);


          map.layers.insert(clusterLayer);



      });

}


function showInfobox(e) {
  var pin = e.target;
//  console.log("target pin");
//  console.log(pin);

  //Check to see if the pushpin is a cluster.
  if (pin.containedPushpins) {
    //Cluster clicked, zoom the map in.
    var locs = [];
    var Sizeofcluster = e.target.containedPushpins;
    for (var i = 0, len = Sizeofcluster.length; i < len; i++) {
      //Get the location of each pushpin.
      locs.push(Sizeofcluster[i].getLocation());
    }
    //Create a bounding box for the pushpins.
    var bounds = Microsoft.Maps.LocationRect.fromLocations(locs);
    //Zoom into the bounding box of the cluster.
    //Add a padding to compensate for the pixel area of the pushpins.
    map.setView({
      bounds: bounds,
      padding: 100
    });
  } else {

  //  console.log(pin.metadata);
  //  console.log(pin.metadata.Latitude);
    var Latitude = pin.metadata.Latitude;
    var Longitude = pin.metadata.Longitude;
    //Pushpin clicked, show infobox.
    infobox.setOptions({
      location: pin.getLocation(),
      title: pin.metadata.Title + " " + "Lattitude : " + Latitude + " " + "Longitude : " + Longitude,
      Latitude: Latitude,
      //htmlContent : "",
	    htmlContent : "<button onclick='tester(" + Latitude +")'>THIS IS A BIG BUTTON</button>",
      description: "Lattitude : " + Latitude + " " + "Longitude : " + Longitude + "" +"        " + "https://www.google.com/",
      //description:'https://www.google.com/',

      visible: true
    });
  }
}


function createFontPushpin(position, text, fontName, fontSizePx, color) {
  var canvas = document.createElement('canvas');
  var canvasText = canvas.getContext('2d');

  //Define font style
  var font = fontSizePx + 'px ' + fontName;
  canvasText.font = font;

  //Resize canvas based on sie of text.
  var size = canvasText.measureText(text);
  canvas.width = size.width;
  canvas.height = fontSizePx;

  //Reset font as it will be cleared by the resize.
  canvasText.font = font;
  canvasText.textBaseline = 'top';
  canvasText.fillStyle = color;

  canvasText.fillText(text, 0, 0);

  return new Microsoft.Maps.Pushpin(position, {
    icon: canvas.toDataURL(),
    anchor: new Microsoft.Maps.Point(canvas.width / 2, canvas.height / 2) //Align center of pushpin with location.
  });
}

