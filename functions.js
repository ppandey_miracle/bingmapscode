function getPins(eachSetofCluster) {
	var val;
  Microsoft.Maps.loadModule("Microsoft.Maps.Clustering",
    function() {
      console.log(clusterloc.length);
      for (var i = 0; i < clusterloc.length; i++) {
        console.log(clusterloc[i].length);
        for (var j = 0; j < clusterloc[i].length; j++) {
          val  = clusterloc[i][j];
		//					console.log(val);
        }
      }
			console.log(val );

			return val;


    });

	}


  function getPushPinsFromPins(p) {
    var PINS = [];

    var val = p;
    // Capturing the long and lat from the list
    var position = new Microsoft.Maps.Location(val.Latitude, val.Longitude);

    // Creating a pushpin
    var pin = new Microsoft.Maps.Pushpin(position, {
      color: "red",
      anchor: new Microsoft.Maps.Point(12, 39)
    });

    pin.metadata = clusterloc[i][j];
    //			console.log (pin.metadata);
    PINS.push(pin);

    return PINS;

  }



  function createClusters() {
    //Create a ClusterLayer with options and add it to the map.
    clusterLayer = new Microsoft.Maps.ClusterLayer(pins);

    //Add a click event to the cluster layer to open an infobox.
    Microsoft.Maps.Events.addHandler(clusterLayer, 'click', showInfobox);

    map.layers.insert(clusterLayer);

    pins = [];

  }



  function showInfobox(e) {
    var pin = e.target;
    console.log("target pin");
    console.log(pin);

    //Check to see if the pushpin is a cluster.
    if (pin.containedPushpins) {
      //Cluster clicked, zoom the map in.
      var locs = [];
      var Sizeofcluster = e.target.containedPushpins;
      for (var i = 0, len = Sizeofcluster.length; i < len; i++) {
        //Get the location of each pushpin.
        locs.push(Sizeofcluster[i].getLocation());
      }

      //Create a bounding box for the pushpins.
      var bounds = Microsoft.Maps.LocationRect.fromLocations(locs);

      //Zoom into the bounding box of the cluster.
      //Add a padding to compensate for the pixel area of the pushpins.
      map.setView({
        bounds: bounds,
        padding: 100
      });
    } else {
      //Pushpin clicked, show infobox.
      infobox.setOptions({
        location: pin.getLocation(),
        title: pin.metadata.Title,
        //		Latitude: pin.metadata.Latitude,
        visible: true
      });
    }
  }
